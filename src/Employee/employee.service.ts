import { Model } from 'mongoose';
import { Inject, Injectable } from '@nestjs/common';
import{Employee, EmployeeSchema} from './employee.schema'
import { AddEmployee } from "./addEmployee";

@Injectable()
export class EmployeeService{

    constructor(
        @Inject('employeeModel')
        private readonly EmployeeModel: Model<Employee>
    ){}
    private readonly employees : Employee[] = [];

    async findAll(): Promise<Employee[]>{
        return await this.EmployeeModel.find().exec();
    }

    async findEmployee(employeeId: string): Promise<Employee>{
        return await this.EmployeeModel.findById(employeeId);
    }

    async addEmployee(addEmployee: AddEmployee): Promise<Employee>{
        const createEmployee = new this.EmployeeModel(addEmployee);
        return await createEmployee.save();
    }

    async editEmployee(addEmployee: AddEmployee, employee: Employee): Promise<Employee>{
        const updateEmployee = employee;
        updateEmployee.name = addEmployee.name;
        updateEmployee.genre = addEmployee.genre;
        return await updateEmployee.save();
    }

    async deleteEmloyee(employee: Employee): Promise<string>{
        employee.remove()
        return "delete successfully";
    }

}