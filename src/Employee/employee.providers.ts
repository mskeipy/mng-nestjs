import { async } from "rxjs/internal/scheduler/async";
import { connection, Connection } from "mongoose";
import { EmployeeSchema } from "./employee.schema";

export const EmployeeProvider = [{
    provider: 'employeeModel',
    useFactory: async (connection: Connection) => connection.model('Employee', EmployeeSchema),
    inject: ['DbConToken']
}]