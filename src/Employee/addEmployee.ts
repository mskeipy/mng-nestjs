export class AddEmployee{
    readonly id: string;
    readonly name: string;
    readonly genre: boolean;
}