import { Document, Schema } from 'mongoose';

export interface Employee extends Document{
    id: string;
    name: string;
    genre: boolean;
}

export const EmployeeSchema = new Schema({
    id: {
        type: String
    },
    name: {
        type: String
    },
    genre: {
        type: Boolean,
        default: false
    }
});