import { Employee } from "./employee.schema";
import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

export interface CustomeRequest extends Request{
    employee: Employee
}
@Injectable()
export class LoggerMiddleWare implements NestMiddleware{
    use(req: Request, res: Response, next: Function){
        console.log('Request...');
        next();
    }
}