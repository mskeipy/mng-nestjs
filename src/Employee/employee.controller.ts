import { Controller, Get, Post, Put, Patch, Delete, Query, Body, Param, Req } from '@nestjs/common';
import { EmployeeService } from './employee.service';
import { Employee } from './employee.schema';
import { AddEmployee } from './addEmployee';

@Controller('api/Employee')
export class EmployeeControler{
    constructor(private readonly employeeService: EmployeeService){}

    @Get()
    async findAll(): Promise<Employee[]>{
        return await this.employeeService.findAll();
    }

    @Post()
    async addEmployee(@Body() addEmployee: AddEmployee): Promise<Employee>{
        return this.employeeService.addEmployee(addEmployee);
    }

    @Get("/:employeeId")
    async getId(@Req() request): Promise<Employee>{
        return request.employee;
    }

    @Put("/:employeeId")
    async editBook( @Req() request): Promise<Employee> {
        return this.employeeService.editEmployee(request.body, request.book);
    }

    @Patch("/:employeeId")
    async patchBook( @Req() request): Promise<Employee> {
        if (request.body._id) {
            delete request.body._id;
        }
        return this.employeeService.editEmployee(request.body, request.book);
    }

    @Delete("/:employeeId")
    async deleteBook( @Req() request): Promise<string> {
        return this.employeeService.deleteEmloyee(request.book);
    }
    
}