import { Module, Global } from "@nestjs/common";
import { EmployeeService } from "./employee.service";
import { EmployeeControler } from "./employee.controller";
import { EmployeeProvider } from "./employee.providers";

@Global()
@Module({
    controllers: [EmployeeControler],
    providers: [EmployeeService],
    exports: [EmployeeService]
})
export class EmployeeModule{
    constructor(private readonly employeeService: EmployeeService){}
}